using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;

namespace Alura.LeilaoOnline.WebApp.Services
{
    public interface IAdminService
    {
        IEnumerable<Leilao> PesquisaLeiloesEmPregoesporTermo(string termo);

        Categoria ConsultaCategoriaComleilaoEmPregaoPorId(int id);
    }
}