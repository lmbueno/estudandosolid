using Microsoft.EntityFrameworkCore;
using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;
using System.Linq;
using Alura.LeilaoOnline.WebApp.Dados.Interface;
using Alura.LeilaoOnline.WebApp.Services;

namespace Alura.LeilaoOnline.WebApp.Dados.Admin
{
    public class DefaultAdminService : IAdminService
    {
        private readonly ILeilaoRepository _leilaoRepository;
        private readonly ICategoriaRepository _categoriaRepository;

        public DefaultAdminService(ILeilaoRepository leilaoRepository, ICategoriaRepository categoriaRepository)
        {
            _leilaoRepository = leilaoRepository;
            _categoriaRepository = categoriaRepository;
        }

        public Categoria ConsultaCategoriaComleilaoEmPregaoPorId(int id)
        {
            return _categoriaRepository.SelectId(id);
        }

        public IEnumerable<Leilao> PesquisaLeiloesEmPregoesporTermo(string termo)
        {
            return _leilaoRepository.SelectTerm(termo);
        }
    }
}