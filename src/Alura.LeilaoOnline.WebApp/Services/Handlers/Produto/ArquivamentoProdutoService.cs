using Microsoft.EntityFrameworkCore;
using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;
using Alura.LeilaoOnline.WebApp.Services;

namespace Alura.LeilaoOnline.WebApp.Dados.Produto
{
    public class ArquivamentoProdutoService : IProdutoService
    {
        DefaultprodutoService _prosutoService;

        public ArquivamentoProdutoService(DefaultprodutoService prosutoService)
        {
            _prosutoService = prosutoService;
        }

        public void CadastraLeilao(Leilao leilao)
        {
            _prosutoService.CadastraLeilao(leilao);
        }

        public IEnumerable<Categoria> ConsultaCategortia()
        {
            return _prosutoService.ConsultaCategortia();
        }

        public IEnumerable<Leilao> ConsultaLeilao()
        {
            return _prosutoService.ConsultaLeilao();
        }

        public Leilao ConsultaLeilaoporId(int id)
        {
            return _prosutoService.ConsultaLeilaoporId(id);
        }

        public void ModificaLeilao(Leilao leilao)
        {
            _prosutoService.ModificaLeilao(leilao);
        }

        public void RemoveLeilao(Leilao leilao)
        {
            _prosutoService.RemoveLeilao(leilao);
        }

        public void ArquivaLeilao(Leilao leilao)
        {
            leilao.Situacao = SituacaoLeilao.Arquivado;
            _prosutoService.ModificaLeilao(leilao);
        }
    }
}