using Microsoft.EntityFrameworkCore;
using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;
using System.Linq;
using Alura.LeilaoOnline.WebApp.Dados.Interface;
using Alura.LeilaoOnline.WebApp.Services;

namespace Alura.LeilaoOnline.WebApp.Dados.Produto
{
    public class DefaultprodutoService : IProdutoService
    {
        private readonly ILeilaoRepository _leilaoRepository;
        private readonly ICategoriaRepository _categoriaRepository;

        public DefaultprodutoService(ILeilaoRepository leilaoRepository, ICategoriaRepository categoriaRepository)
        {
            _leilaoRepository = leilaoRepository;
            _categoriaRepository = categoriaRepository;
        }

        public void CadastraLeilao(Leilao leilao)
        {
            _leilaoRepository.Create(leilao);
        }

        public IEnumerable<Categoria> ConsultaCategortia()
        {
            return _categoriaRepository.SelectAll();
        }

        public IEnumerable<Leilao> ConsultaLeilao()
        {
            return _leilaoRepository.SelectAll();
        }

        public Leilao ConsultaLeilaoporId(int id)
        {
            return _leilaoRepository.SelectId(id);
        }

        public void ModificaLeilao(Leilao leilao)
        {
            _leilaoRepository.Update(leilao);
        }

        public void RemoveLeilao(Leilao leilao)
        {
            _leilaoRepository.Delete(leilao);
        }
    }
}