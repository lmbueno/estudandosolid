
namespace Alura.LeilaoOnline.WebApp.Dados.Interface
{
    public interface ICommand<T> 
    {
        void Create(T obj);

        void Update(T obj);

        void Delete(T obj);
    }
}