using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;

namespace Alura.LeilaoOnline.WebApp.Dados.Interface
{
    public interface ICategoriaRepository : IQuery<Categoria>
    {
        IEnumerable<Categoria> SelectCategoriaLeilao();
    }
}