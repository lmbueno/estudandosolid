using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;

namespace Alura.LeilaoOnline.WebApp.Dados.Interface
{
    public interface ILeilaoRepository : ICommand<Leilao>, IQuery<Leilao>
    {
        IEnumerable<Leilao> SelectTerm(string termo);
    }
}