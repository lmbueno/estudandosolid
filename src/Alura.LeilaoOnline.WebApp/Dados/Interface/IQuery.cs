using System.Collections.Generic;

namespace Alura.LeilaoOnline.WebApp.Dados.Interface
{
    public interface IQuery<T>
    {
        IEnumerable<T> SelectAll();        

        T SelectId(int id);        
    }
}