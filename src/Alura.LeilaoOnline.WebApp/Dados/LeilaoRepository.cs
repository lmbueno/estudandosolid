using Microsoft.EntityFrameworkCore;
using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;
using System.Linq;
using Alura.LeilaoOnline.WebApp.Dados.Interface;

namespace Alura.LeilaoOnline.WebApp.Dados
{
    public class LeilaoRepositopry : ILeilaoRepository
    {
        AppDbContext _context;

        public LeilaoRepositopry()
        {
            _context = new AppDbContext();
        }

        public IEnumerable<Leilao> SelectAll()
        {
            return _context.Leiloes
                .Include(l => l.Categoria)
                .ToList();
        }

        public IEnumerable<Leilao> SelectTerm(string termo)
        {
            return _context.Leiloes
                    .Include(l => l.Categoria)
                    .Where(l => string.IsNullOrWhiteSpace(termo) ||
                        l.Titulo.ToUpper().Contains(termo.ToUpper()) ||
                        l.Descricao.ToUpper().Contains(termo.ToUpper()) ||
                        l.Categoria.Descricao.ToUpper().Contains(termo.ToUpper())
                  );
        }

        public Leilao SelectId(int id)
        {
            return _context.Leiloes.Find(id);
        }

        public void Create(Leilao leilao)
        {
            _context.Leiloes.Add(leilao);
            _context.SaveChanges();
        }

        public void Update(Leilao leilao)
        {
            _context.Leiloes.Update(leilao);
            _context.SaveChanges();
        }

        public void Delete(Leilao leilao)
        {
            _context.Leiloes.Remove(leilao);
            _context.SaveChanges();
        }
    }
}