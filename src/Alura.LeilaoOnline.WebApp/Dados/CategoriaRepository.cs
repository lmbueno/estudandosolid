using Microsoft.EntityFrameworkCore;
using Alura.LeilaoOnline.WebApp.Models;
using System.Collections.Generic;
using System.Linq;
using Alura.LeilaoOnline.WebApp.Dados.Interface;

namespace Alura.LeilaoOnline.WebApp.Dados
{
    public class CategoriaRepository : ICategoriaRepository
    {
        AppDbContext _context;

        public CategoriaRepository()
        {
            _context = new AppDbContext();
        }

        public IEnumerable<Categoria> SelectAll()
        {
            return _context.Categorias.ToList();
        }

        public IEnumerable<Categoria> SelectCategoriaLeilao()
        {
            return _context.Categorias
                    .Include(c => c.Leiloes)
                    .Select(c => new CategoriaComInfoLeilao
                    {
                        Id = c.Id,
                        Descricao = c.Descricao,
                        Imagem = c.Imagem,
                        EmRascunho = c.Leiloes.Where(l => l.Situacao == SituacaoLeilao.Rascunho).Count(),
                        EmPregao = c.Leiloes.Where(l => l.Situacao == SituacaoLeilao.Pregao).Count(),
                        Finalizados = c.Leiloes.Where(l => l.Situacao == SituacaoLeilao.Finalizado).Count(),
                    });
        }

        public Categoria SelectId(int categoria)
        {
            return _context.Categorias
                    .Include(c => c.Leiloes)
                    .First(c => c.Id == categoria);
        }

        public IEnumerable<Categoria> SelectTerm(string termo)
        {
            throw new System.NotImplementedException();
        }
    }
}